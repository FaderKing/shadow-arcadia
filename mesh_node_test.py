from direct.showbase.ShowBase import ShowBase
from panda3d.core import loadPrcFileData
from panda3d.core import Filename
from panda3d.core import AmbientLight, Vec4, DirectionalLight

def getPandaPath(path):
    new_path = Filename.fromOsSpecific(path)
    return new_path

model_path = getPandaPath("C:/Users/ACER/Desktop/Code/GameDev/Python/shadow-arcadia/")
print(model_path)

config_vars = f"""
win-size 1280 720
window-title Testing Application
model-path ${model_path}
"""

loadPrcFileData("", config_vars)
class TestApp(ShowBase):
    def __init__(self):
        ShowBase.__init__(self)

        self.scene = loader.loadModel("assets/props/scene.egg")
        self.object = loader.loadModel("assets/props/object.egg")
        self.scene.reparentTo(render)
        self.object.reparentTo(render)
        self.object.hide()

        ambience = AmbientLight('ambience')
        ambience.setColor(Vec4(0.2, 0.2, 0.2, 1))
        self.ambienceNp = render.attachNewNode(ambience)
        render.setLight(self.ambienceNp)

        dirLight = DirectionalLight("dirLight")
        dirLight.setColor(Vec4(0.4, 0.35, 0.35, 1))
        self.dirLightNp = render.attachNewNode(ambience)
        self.dirLightNp.lookAt(self.object)
        render.setLight(self.dirLightNp)

        handle = self.scene.find("NewNode")
        self.object.reparentTo(handle)
        self.object.show()
        self.camera.setPos(0,0,5)
        self.camera.setHpr(0,45,0)

        render.setShaderAuto()

app = TestApp()
app.run()