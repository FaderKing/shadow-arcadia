from core import DiscordIntegration
from core import getSecretFromEnv
from core import GameData, GameIO
from core import Player
from core import NetPacket

from panda3d.core import AmbientLight
from panda3d.core import DirectionalLight
from panda3d.core import Vec4, Vec3

class Game:
    def __init__(self):
         # Will be updated and relayed to player integrations,
         # e.g. Discord
        self.gameAttributes = {
            # GameTitle: "Title of the current Gamemode"
            "GameTitle": "",
            # Headcount: [alive, dead]
            "HeadCount": [],
            # Party Size: [filled, available]
            "partySize": [],
            # DuoTeammateGamertags: "Player1 x Player2" 
            "DuoTeammateGamertags": "",
            # JoinMPGameID: ID of the current Multiplayer session
            "JoinMPGameID": "Unavailable",
            # Epoch Time of when the Game Started
            "StartTime": "Unavailable",
            # Time in seconds of when the Game should end
            "EndTime": "Unavailable",
            # The current Map a Game is being played in
            "Map": ""
        }

        self.environment = loader.loadModel("assets/environment/pvp_map_unorganized/map_bam.bam")
        self.environment.setPos(0, 0, -1)
        self.environment.reparentTo(render)

        #self.disableMouse()

        self.player = Player()
        self.player.avatar.setScale(0.06)
        self.player.avatar.setPos(0, 0, -0.82)
        self.player.avatar.reparentTo(render)
        
        self.camera.setPos(self.player.avatar.getPos() + Vec3(0, -2, 2))
        self.camera.lookAt(self.player.avatar)
        
        ambience = AmbientLight('ambience')
        ambience.setColor(Vec4(0.1, 0.1, 0.1, 1))
        self.ambienceNp = render.attachNewNode(ambience)
        render.setLight(self.ambienceNp)

        mainLight = DirectionalLight("main_light")
        self.mainLightNp = render.attachNewNode(mainLight)
        self.mainLightNp.setHpr(45, -45, 0)
        render.setLight(self.mainLightNp)

        render.setShaderAuto()