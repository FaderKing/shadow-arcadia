#!/usr/bin/env python

from panda3d.core import *

# Tell Panda3D to use OpenAL, not FMOD
config_vars = f"""
window-title Shadow Arcadia: Playlist Demo
audio-library-name p3openal_audio
win-size 1280 720
"""
loadPrcFileData("", config_vars)

from direct.showbase.DirectObject import DirectObject
from direct.gui.OnscreenText import OnscreenText
from direct.showbase.ShowBase import ShowBase

# Function to put title on the screen.
def addTitle(text):
    font = loader.loadFont("assets/fonts/main_font.ttf")
    return OnscreenText(text=text, style=1, pos=(-0.1, 0.09), scale=.06,
                        parent=base.a2dBottomRight, align=TextNode.ARight,
                        fg=(1, 1, 1, 1), shadow=(0, 0, 0, 1), font=font)


class MediaPlayer(ShowBase):

    def __init__(self, media_file):
        # Initialize the ShowBase class from which we inherit, which will
        # create a window and set up everything we need for rendering into it.
        ShowBase.__init__(self)

        # Load the texture. We could use loader.loadTexture for this,
        # but we want to make sure we get a MovieTexture, since it
        # implements synchronizeTo.
        self.tex = MovieTexture("name")
        success = self.tex.read(media_file)
        assert success, "Failed to load video!"

        # Set up a fullscreen card to set the video texture on.
        cm = CardMaker("My Fullscreen Card")
        cm.setFrameFullscreenQuad()
        addTitle("Alpha Build: 0.11.9")

        # Tell the CardMaker to create texture coordinates that take into
        # account the padding region of the texture.
        cm.setUvRange(self.tex)

        # Now place the card in the scene graph and apply the texture to it.
        card = NodePath(cm.generate())
        card.reparentTo(self.render2d)
        card.setTexture(self.tex)

        self.sound = loader.loadSfx("audio/playlists/test/Paradise - FK Remix.mp3")
        # Synchronize the video to the sound.
        self.tex.synchronizeTo(self.sound)

        self.accept('p', self.playpause)
        self.accept('P', self.playpause)
        self.accept('s', self.stopsound)
        self.accept('S', self.stopsound)

    def stopsound(self):
        self.sound.stop()
        self.sound.setPlayRate(1.0)

    def playpause(self):
        if self.sound.status() == AudioSound.PLAYING:
            t = self.sound.getTime()
            self.sound.stop()
            self.sound.setTime(t)
        else:
            self.sound.play()

player = MediaPlayer("assets/videos/loading.mp4")
player.run()
