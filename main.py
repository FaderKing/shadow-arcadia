# By Zikora Ogbuagu
from direct.showbase.ShowBase import ShowBase

from panda3d.core import WindowProperties
from panda3d.core import loadPrcFileData
from panda3d.core import Filename

# clientID = getSecretFromEnv("clientID")
# print("[RPC:] Acquired Application ID")

# discordInt = DiscordIntegration(clientID)
# discordInt.connectToService()
# discordInt.updateGameState()

def getPandaPath(path):
    new_path = Filename.fromOsSpecific(path)
    return new_path

model_path = getPandaPath("C:/Users/ACER/Desktop/Code/GameDev/Python/shadow-arcadia/")
print(model_path)

config_vars = f"""
win-size 1280 720
window-title Shadow Arcadia: Prototype
model-path ${model_path}
"""

loadPrcFileData("", config_vars)

class LauncherMain(ShowBase):
    def __init__(self):
        super().__init__()

        self.startUpWindow((1280, 720))
        self.updateTask = taskMgr.add(self.update, "update")

    def startUpWindow(self, resolution):
        """Starts the game window based on the following args:

        Args:
            resolution (tuple): The vertical and horizontal pixels that the game window should
            be set to.
        """
        properties = WindowProperties()
        properties.setSize(resolution[0], resolution[1])
        self.win.requestProperties(properties)

    def startFileIO(self):
        self.gameIO = GameIO()

    def update(self, task):
        dt  = globalClock.getDt()
        return task.cont

if __name__ == "__main__":
    game = Game()
    game.run()