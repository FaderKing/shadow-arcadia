from direct.fsm.FSM import FSM
from direct.actor.Actor import Actor
from direct.showbase.ShowBase import ShowBase
from direct.showbase.DirectObject import DirectObject
from direct.gui.OnscreenText import OnscreenText
from panda3d.core import (AmbientLight,
    DirectionalLight,
    Filename,
    KeyboardButton,
    MovieTexture,
    Vec3,
    Vec4,
    CardMaker,
    NodePath,
    loadPrcFileData,
    TextNode)
import time

class Controls:
    def __init__(self):
        self.keys = {
            "up": KeyboardButton.asciiKey("w"),
            "down": KeyboardButton.asciiKey("s"),
            "left": KeyboardButton.asciiKey("a"),
            "right": KeyboardButton.asciiKey("d"),
            "sprint": KeyboardButton.shift(),
            "crouch": KeyboardButton.asciiKey("c")
        }

class Khronos:
    def __init__(self):
        self.clocks = {}

    def addClock(self, name):
        self.clocks[name] = GlobalClock.getFrameTime()
        return self.clocks[name]

    def removeClock(self, name):
        del self.clocks[name]
    
    def getClocks(self):
        return self.clocks

class Player(FSM):
    def __init__(self, avatarName, username):
        self.username = username
        FSM.__init__(self, "PlayerFSM")

        # avatarName = "apollo"
        relativePath = f"assets/avatars/{avatarName}/"
        self.avatar = Actor(relativePath + f"{avatarName}.egg",
		{
			"slow_walk": f"{relativePath}/{avatarName}_slow_walk.egg",
			"sneak_walk": f"{relativePath}/{avatarName}_sneak_walk.egg",
			"turn_around": f"{relativePath}/{avatarName}_turn_around.egg",
			"wall_climb": f"{relativePath}/{avatarName}_wall_climb.egg",
			"run_to_slide": f"{relativePath}/{avatarName}_run_to_slide.egg",
			"jump_forward": f"{relativePath}/{avatarName}_jump_forward.egg",
			"rifle_aim_sprint": f"{relativePath}/{avatarName}_rifle_aim_sprint.egg",
			"rifle_sprint": f"{relativePath}/{avatarName}_rifle_sprint.egg",
			"rifle_idle": f"{relativePath}/{avatarName}_rifle_idle.egg",
			"rifle_idle_aiming": f"{relativePath}/{avatarName}_rifle_idle_aiming.egg",
			"rifle_crouch_aiming": f"{relativePath}/{avatarName}_rifle_crouch_aiming.egg",
			"rifle_crouch_idle": f"{relativePath}/{avatarName}_rifle_crouch_idle.egg",
			"sprint": f"{relativePath}/{avatarName}_rifle_sprint.egg",
			"dive_r": f"{relativePath}/{avatarName}_rifle_dive_r.egg",
		})

        self.physAttr = {
            "mass": 20.0,
            "sprint_accel": 8.0,
            "walk_velInc": 4.0,
            "sneak_velInc": 2.5,
            "velocity": 0.0,
            "max_vel": 100.0
        }

        self.physMod = {
            "isMoving": False,
            "crouch": False,
            "sprint": False
        }
        self.timeMgr = Khronos()
        self.controls = Controls()

        self.avatar.setH(90)
        self.avatar.reparentTo(render)
        self.avatar.hide()

    def updateCrouch(self):
        if self.physMod["crouch"]:
            self.physMod["crouch"] = False
        else:
            self.physMod["crouch"] = True

    def movementTask(self, task):
        dt = globalClock.getDt()
        positionDeltaVector = Vec3(0, 0, 0)
        isDown = base.mouseWatcherNode.isButtonDown

        if isDown(self.controls.keys["up"]):
            if isDown(self.controls.keys["sprint"]):
                self.physMod["sprint"] = True
                self.physAttr["velocity"] += self.physAttr["sprint_accel"] * (dt * dt)
                positionDeltaVector = Vec3(0, self.physAttr["velocity"] * dt, 0)
            elif self.physMod["crouch"]:
                positionDeltaVector = Vec3(0, self.physAttr["sneak_velInc"] * dt, 0)
            else:
                positionDeltaVector = Vec3(0, self.physAttr["walk_velInc"] * dt, 0)

            self.physMod["isMoving"] = True

        if isDown(self.controls.keys["down"]):
            if isDown(self.controls.keys["sprint"]):
                self.physMod["sprint"] = True
                self.physAttr["velocity"] += self.physAttr["sprint_accel"] * (dt * dt)
                positionDeltaVector = Vec3(0, -self.physAttr["velocity"] * dt, 0)
            elif self.physMod["crouch"]:
                positionDeltaVector = Vec3(0, -self.physAttr["sneak_velInc"] * dt, 0)
            else:
                positionDeltaVector = Vec3(0, -self.physAttr["walk_velInc"] * dt, 0)
                
            self.physMod["isMoving"] = True

        if isDown(self.controls.keys["left"]):
            if isDown(self.controls.keys["sprint"]):
                self.physMod["sprint"] = True
                self.physAttr["velocity"] += self.physAttr["sprint_accel"] * (dt * dt)
                positionDeltaVector = Vec3(-self.physAttr["velocity"] * dt, 0)
            elif self.physMod["crouch"]:
                positionDeltaVector = Vec3(-self.physAttr["sneak_velInc"] * dt, 0, 0)
            else:
                positionDeltaVector = Vec3(-self.physAttr["walk_velInc"] * dt, 0, 0)

            self.physMod["isMoving"] = True
                

        if isDown(self.controls.keys["right"]):
            if isDown(self.controls.keys["sprint"]):
                self.physMod["sprint"] = True
                self.physAttr["velocity"] += self.physAttr["sprint_accel"] * (dt * dt)
                positionDeltaVector = Vec3(self.physAttr["velocity"] * dt, 0)
            elif self.physMod["crouch"]:
                positionDeltaVector = Vec3(self.physAttr["sneak_velInc"] * dt, 0, 0)
            else:
                positionDeltaVector = Vec3(self.physAttr["walk_velInc"] * dt, 0, 0)

            self.physMod["isMoving"] = True

        self.avatar.setPos(self.avatar, positionDeltaVector)

        if not self.physMod["isMoving"] and self.state != "Walk" and self.state != "Sprint":
            self.physMod["isMoving"] = False
            self.request("Idle")
        
        if isDown(self.controls.keys["up"]):
            if isDown(self.controls.keys["sprint"]):
                self.request("Sprint")
            self.avatar.setH(0)
        
        if isDown(self.controls.keys["down"]):
            if isDown(self.controls.keys["sprint"]):
                self.request("Sprint")
            self.avatar.setH(90)

        if isDown(self.controls.keys["left"]):
            if isDown(self.controls.keys["sprint"]):
                self.request("Sprint")
            self.avatar.setH(180)

        if isDown(self.controls.keys["right"]):
            if isDown(self.controls.keys["sprint"]):
                self.request("Sprint")
            self.avatar.setH(270)

        return task.cont

    def start(self, startPos):
        self.avatar.setPos(startPos)
        self.avatar.setScale(0.125)
        taskMgr.add(self.movementTask, f"move task {self.username}")
        self.avatar.show()
    
    def stop(self):
        self.avatar.hide()
        taskMgr.remove(f"move task {self.username}")

    def enterWalk(self):
        self.avatar.loop("slow_walk")

    def exitWalk(self):
        self.avatar.stop()

    def enterSprint(self):
        self.avatar.loop("sprint")

    def exitSprint(self):
        self.avatar.stop()

    def enterIdle(self):
        self.avatar.loop("rifle_idle")

    def exitIdle(self):
        self.avatar.stop()

def getPandaPath(path):
    new_path = Filename.fromOsSpecific(path)
    return new_path

model_path = getPandaPath("C:/Users/ACER/Desktop/Code/GameDev/Python/shadow-arcadia/")
print(model_path)

config_vars = f"""
win-size 1280 720
window-title Shadow Arcadia: Player FSM
model-path ${model_path}
"""

loadPrcFileData("", config_vars)

class PlayerDemo(ShowBase):
    def __init__(self):
        super().__init__(self)

        self.player = Player("apollo", "FaderKing")
        self.accept("c", self.player.updateCrouch)
        self.world = loader.loadModel("assets\environment\pvp_map_unorganized\map_bam.bam")
        self.player.start((0, 0, 0))
        self.world.setH(90)
        self.world.reparentTo(render)

        self.camera.setPos(self.player.avatar.getPos() + Vec3(0, -2, 2))
        self.camera.lookAt(self.player.avatar)
        
        ambience = AmbientLight('ambience')
        ambience.setColor(Vec4(0.1, 0.1, 0.1, 1))
        self.ambienceNp = render.attachNewNode(ambience)
        render.setLight(self.ambienceNp)

        mainLight = DirectionalLight("main_light")
        self.mainLightNp = render.attachNewNode(mainLight)
        self.mainLightNp.setHpr(45, -45, 0)
        render.setLight(self.mainLightNp)

        render.setShaderAuto()

demo = PlayerDemo()
demo.run()