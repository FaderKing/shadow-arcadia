from pypresence import Presence
from pypresence import Client

import pickle
import json

from direct.actor.Actor import Actor
from direct.fsm.FSM import FSM
from panda3d.core import KeyboardButton

class Controls:
	def __init__(self):
		self.keys = {
			"up": KeyboardButton.ascii_key("w"),
			"left": KeyboardButton.ascii_key("a"),
			"right": KeyboardButton.ascii_key("d"),
			"down": KeyboardButton.ascii_key("s"),
			"crouch": KeyboardButton.ascii_key("c"),
			"sprint": KeyboardButton.ascii_key("l"),
			"jump": KeyboardButton.ascii_key("j"),
		}

class Player(FSM):
	def __init__(self, playerID=None):
		FSM.__init__(self, "FSM-Player{}".format(playerID))
		if not playerID:
			avatarName = "apollo"
			relativePath = "assets/avatars/apollo/"
			self.avatar = Actor(relativePath + "apollo.egg",
			{
				"slow_walk": f"{relativePath}/{avatarName}_slow_walk.egg",
				"sneak_walk": f"{relativePath}/{avatarName}_sneak_walk.egg",
				"turn_around": f"{relativePath}/{avatarName}_turn_around.egg",
				"wall_climb": f"{relativePath}/{avatarName}_wall_climb.egg",
				"run_to_slide": f"{relativePath}/{avatarName}_run_to_slide.egg",
				"jump_forward": f"{relativePath}/{avatarName}_jump_forward.egg",
				"rifle_aim_sprint": f"{relativePath}/{avatarName}_rifle_aim_sprint.egg",
				"rifle_sprint": f"{relativePath}/{avatarName}_rifle_sprint.egg",
				"rifle_idle": f"{relativePath}/{avatarName}_rifle_idle.egg",
				"rifle_idle_aiming": f"{relativePath}/{avatarName}_rifle_idle_aiming.egg",
				"rifle_crouch_aiming": f"{relativePath}/{avatarName}_rifle_crouch_aiming.egg",
				"rifle_crouch_idle": f"{relativePath}/{avatarName}_rifle_crouch_idle.egg",
				"sprint": f"{relativePath}/{avatarName}_rifle_sprint.egg",
				"dive_r": f"{relativePath}/{avatarName}_rifle_dive_r.egg",
			})

			self.physAttr = {
				"mass": 400,
				"acceleration": None,
				"velocityStep": 2.0,
				"maxspeed": None
			}

			self.physMod = {
				"isMoving": False,
				"isCrouching": False,
				"isAiming": False,
				"isDiving": False
			}

			self.controls = Controls()

	# Print statements will be removed
	def updatephysAttr(self, controlName, controlState):
		self.physAttr[controlName] = controlState
		print("[PA]", controlName, " set to ", controlState)

	def updatephysMod(self, controlName, controlState):
		self.physMod[controlName] = controlState
		print("[PM]", controlName, " set to ", controlState)

	# Start FSM Functionality
	def movementTask(self, task):
		velocity = 0.0
		isDown = base.mouseWatcherNode.isButtonDown
		if isDown(self.controls.keys["up"]):
			velocity += self.physAttr["velocityStep"]
		if isDown(self.controls.keys["left"]):
			pass
		if isDown(self.controls.keys["right"]):
			pass
		if isDown(self.controls.keys["down"]):
			pass
		if isDown(self.controls.keys["jump"]):
			pass
		if isDown(self.controls.keys["crouch"]):
			pass
		if isDown(self.controls.keys["sprint"]):
			pass

class Integration:
	"""
	Represents a handler of any external but supported
	application or framework. This class is used strictly
	by inheritance
	"""
	def __init__(self):
		pass

	def connectToService(self):
		pass

	def disconnectFromService(self):
		pass

	def updateGameState(self, state):
		pass

	def updatePlayerData(self, data):
		pass

	def uploadPlayerData(self, data):
		pass

	def unlockAchievement(self, achievementID):
		pass

def getSecretFromEnv(secretId):
	file = open("secrets.kos", "r")
	for line, content in enumerate(file):
		if content.split("//")[1].split(":")[0] == secretId:
			return content.split("//")[1].split(":")[1]

class DiscordIntegration(Integration):
	def __init__(self, clientID):
		self.client = Presence(clientID)

	def connectToService(self):
		self.client.connect()

	def disconnectFromService(self):
		try:
			self.client.clear()
		except Exception as e:
			pass
		self.client.close()

	def updateGameState(self, state):
		self.client.update(
			state=state["state"],
			details=state["details"],
			start=state["startTime"],
			end=state["endTime"],
			large_image=state["limage"],
			large_text=state["ltext"],
			small_image=state['simage'],
			small_text=state['stext'],
			party_id=state["partyID"],
			party_size=state["partySize"],
			spectate=state["spectateHash"],
			join=state['joinHash'],
			instance=state["instance"]
		)

class NetPacket(object):
	def __init__(self, content):
		self.content = content
		
	def serialize(self):
		return pickle.dumps(self)

class GameIO:
	def __init__(self):
		import os
		if os.path.isdir(os.getcwd() + "/user"):
			self.path = os.getcwd() + "/user"
		else:
			os.mkdir("user")
			self.path = os.getcwd() + "/user"

		os.chdir(os.getcwd() + "/user")

	def dump(self, data, filename):
		file = open(filename+".kos", "wb")
		pickle.dump(data, file)
		file.close()

	def loads(self, filename):
		file = open(filename+".kos", "rb")
		obj = pickle.load(file)
		file.close()
		return obj

class GameData:
	def __init__(self, data, header):
		self.header = header
		self.data = data

class PlayerGroup:
	"""docstring for PlayerGroup"""
	def __init__(self, size: int, members: dict, factor: str, isFinite=False):
		self.isFinite = isFinite
		self.size = size
		self.members = members
		self.factor = factor
