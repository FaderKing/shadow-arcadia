from direct.showbase.ShowBase import ShowBase
from direct.gui.OnscreenImage import OnscreenImage
from direct.task.Task import Task
from panda3d.core import loadPrcFileData
import os
import json

config_vars = f"""
win-size 1280 720
window-title Shadow Arcadia: Playlist Demo
"""

loadPrcFileData("", config_vars)

class PlaylistDemo(ShowBase):
    def __init__(self, playlistTitle):
        super().__init__(self)

        frame = OnscreenImage(image="assets/images/frame_1.jpg", pos=(0, 0, 0), scale=(1.75, 1, 1))
        self.playlist = {}
        self.path = "audio/playlists/{0}".format(playlistTitle)
        self.playlist = self.assignTracks(self.playlist)

        self.currentTrack = {
            "title": None,
            "index": None,
            "object": None,
            "path": None
        }

        self.accept("arrow_right-up", self.nextTrack)
        self.accept("arrow_left-up", self.prevTrack)
        self.accept("p-up", self.printPlaylist)

        taskMgr.add(self.updateTracks, "trackUpdateTask")
        self.startPlaylist()

    def updateTracks(self, task):
        if int(self.currentTrack["index"]) != len(self.playlist):
            if self.currentTrack["object"].status() == self.currentTrack["object"].PLAYING:
                return task.cont
            else:
                self.updateCurrent(int(self.currentTrack["index"]) + 1)
                self.currentTrack["object"].play()
                return task.cont
        else:
            return task.done

    def assignTracks(self, dict):
        if os.path.exists(self.path) and os.listdir(self.path):
            index = 1
            for i in range(len(os.listdir(self.path))):
                dict[str(index)] = os.listdir(self.path)[index - 1]
                index += 1
        return dict

    def updateCurrent(self, index):
        self.currentTrack["index"] = index
        self.currentTrack["title"] = self.playlist[str(index)]
        self.currentTrack["path"] = "{0}/{1}".format(self.path, self.currentTrack["title"])
        self.currentTrack["object"] = base.loader.loadSfx(self.currentTrack["path"])
        print("[#] Now Playing: {0}".format(self.currentTrack["title"].split(".mp3")[0]))

    def startPlaylist(self):
        self.updateCurrent(1)
        self.currentTrack["object"].play()

    # TODO: Add repeat toggle keyboard event
    def nextTrack(self):
        if self.currentTrack["index"] != self.playlist[str(len(self.playlist)-1)]:
            self.currentTrack["object"].stop()
            self.updateCurrent(int(self.currentTrack["index"]))
        else:
            self.currentTrack["object"].play()
            print("[#] End of playlist")

    def prevTrack(self):
        if self.currentTrack["index"] != self.playlist["1"]:
            self.currentTrack["object"].stop()
            self.updateCurrent(int(self.currentTrack["index"]) - 1)
        else:
            self.currentTrack["object"].play()
            print("[#] Start of playlist")

    def printPlaylist(self):
        print(json.dumps(self.playlist, indent=4))

demo = PlaylistDemo("test")
demo.run()